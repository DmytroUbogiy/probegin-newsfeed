package com.ubogiy.dmytro.probeginapp.event;

/**
 * Created by dmytroubogiy on 23.06.17.
 */

public class ErrorApiEvent {
    public String message;
    public String title;
}
