package com.ubogiy.dmytro.probeginapp.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.ubogiy.dmytro.probeginapp.ProBeginApp;
import com.ubogiy.dmytro.probeginapp.model.NewsModel;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * Created by dmytroubogiy on 24.06.17.
 */

public class LoadImage {

    private static LoadImage instance;
    private ImageLoader imageLoader;

    private LoadImage() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(ProBeginApp.getContext()));
    }

    public static LoadImage getInstance() {
        if (instance == null) {
            instance = new LoadImage();
        }
        return instance;
    }

    public void loadImageWithUrl(String url, final ImageAware imageView, final HashMap<NewsModel, Bitmap> loadedImages, final NewsModel item) {
        imageLoader.loadImage(url, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                super.onLoadingStarted(imageUri, view);
                imageView.setImageBitmap(null);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imageView.setImageBitmap(loadedImage);
                loadedImages.put(item, loadedImage);
            }
        });
    }
}
