package com.ubogiy.dmytro.probeginapp.model;

/**
 * Created by dmytroubogiy on 23.06.17.
 */

public class NewsModel {
    public String imageUrl;
    public String title;
    public String details;
}
