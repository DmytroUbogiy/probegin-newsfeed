package com.ubogiy.dmytro.probeginapp.activity.news;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ubogiy.dmytro.probeginapp.R;

/**
 * Created by dmytroubogiy on 24.06.17.
 */

public class NewsViewHolder {
    public ImageView newsLogo;
    public TextView newsTitle;
    public Button readMore;
    public ProgressBar progressBar;

    public NewsViewHolder(View view) {
        newsLogo = (ImageView) view.findViewById(R.id.imageViewNewsLogo);
        newsTitle = (TextView) view.findViewById(R.id.textViewTitle);
        readMore = (Button) view.findViewById(R.id.buttonReadMore);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    }
}
