package com.ubogiy.dmytro.probeginapp.activity.news;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.ubogiy.dmytro.probeginapp.ProBeginApp;
import com.ubogiy.dmytro.probeginapp.R;
import com.ubogiy.dmytro.probeginapp.model.NewsModel;
import com.ubogiy.dmytro.probeginapp.util.LoadImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dmytroubogiy on 24.06.17.
 */

public class NewsAdapter extends BaseAdapter {

    private List<NewsModel> newsItems;
    private LayoutInflater inflater;
    private HashMap<NewsModel, Bitmap> loadedImages;
    private ImageLoader imageLoader;

    public NewsAdapter(Context context, List<NewsModel> newsItems) {
        this.newsItems = new ArrayList<>();
        this.newsItems.addAll(newsItems);
        inflater = LayoutInflater.from(context);
        loadedImages = new HashMap<>();
        for (int i = 0; i < newsItems.size(); ++i) {
            loadedImages.put(newsItems.get(i), null);
        }
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(ProBeginApp.getContext()));
    }

    @Override
    public int getCount() {
        return newsItems.size();
    }

    @Override
    public NewsModel getItem(int position) {
        return newsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final NewsViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.news_item, parent, false);
            viewHolder = new NewsViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (NewsViewHolder) convertView.getTag();
        }
        final NewsModel item = getItem(position);
        viewHolder.newsTitle.setText(item.title);
        viewHolder.newsLogo.setImageBitmap(null);
        if (loadedImages.get(item) == null) {
            final ImageAware image = new ImageViewAware(viewHolder.newsLogo);
            imageLoader.loadImage(item.imageUrl, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    super.onLoadingStarted(imageUri, view);
                    viewHolder.newsLogo.setVisibility(View.INVISIBLE);
                    viewHolder.progressBar.setVisibility(View.VISIBLE);
                }


                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    viewHolder.newsLogo.setVisibility(View.VISIBLE);
                    viewHolder.progressBar.setVisibility(View.INVISIBLE);
                    image.setImageBitmap(loadedImage);
                    loadedImages.put(item, loadedImage);
                }
            });
        }
        else {
            viewHolder.newsLogo.setImageBitmap(loadedImages.get(item));
        }
        return convertView;
    }

    public void addNewsToAdapter(List<NewsModel> news) {
        newsItems.addAll(news);
        for(int i = 0; i < news.size(); ++i) {
            loadedImages.put(news.get(i), null);
        }
        notifyDataSetChanged();
    }
}
