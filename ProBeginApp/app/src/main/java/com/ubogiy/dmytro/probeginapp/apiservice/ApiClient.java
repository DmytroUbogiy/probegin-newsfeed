package com.ubogiy.dmytro.probeginapp.apiservice;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ubogiy.dmytro.probeginapp.event.ErrorApiEvent;
import com.ubogiy.dmytro.probeginapp.event.GetNewsEvent;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by dmytroubogiy on 23.06.17.
 */

public class ApiClient {

    private static ApiClient instance;
    private String baseUrl;
    private Retrofit retrofit;
    private RestService service;

    private ApiClient() {
        this.baseUrl = "https://www.probegin.com/";
        OkHttpClient.Builder httpClient = createClient();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Content-Type", "text/html")
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(logging);
        Gson gson = new GsonBuilder()
                //.registerTypeAdapter(DateTime.class, new DotNetDateTimeAdapter())
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(RestService.class);
    }

    public static ApiClient getInstance() {
        if (instance == null) {
            instance = new ApiClient();
        }
        return instance;
    }

    public void requestNews(int page) {
        Call call = service.getNews(page);
        new RequestCalls().execute(call);
    }

    public void requestNewDetail() {
        Call call = service.getNewsDetail();
        new RequestCalls().execute(call);
    }

    private class RequestCalls extends AsyncTask<Call, Void, Void> {

        @Override
        protected Void doInBackground(Call... baseRequests) {

            baseRequests[0].enqueue(new Callback() {
                @Override
                public void onResponse(Response response) {
                    if (response.code() == 200) {
                        GetNewsEvent news = new GetNewsEvent();
                        try {
                            news.html = ((ResponseBody)response.body()).string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().postSticky(news);
                    }
                    else {
                        ErrorApiEvent error = new ErrorApiEvent();
                        error.title = "Connection error";
                        error.message = "Failed with code: " + response.code();
                    }
                    Log.d("COOOOOOOOOODE", response.message());
                }

                @Override
                public void onFailure(Throwable t) {
                    ErrorApiEvent error = new ErrorApiEvent();
                    error.title = "Connection error";
                    error.message = t.getMessage();
                    EventBus.getDefault().postSticky(error);
                }
            });

            return null;
        }

    }

    private OkHttpClient.Builder createClient() {
        final OkHttpClient.Builder client = new OkHttpClient().newBuilder();
        client.readTimeout(30, TimeUnit.SECONDS);
        client.connectTimeout(30, TimeUnit.SECONDS);
        return client;
    }
}
