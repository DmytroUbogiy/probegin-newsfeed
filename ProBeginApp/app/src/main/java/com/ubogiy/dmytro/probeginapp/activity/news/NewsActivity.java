package com.ubogiy.dmytro.probeginapp.activity.news;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.ubogiy.dmytro.probeginapp.R;
import com.ubogiy.dmytro.probeginapp.apiservice.ApiClient;
import com.ubogiy.dmytro.probeginapp.event.ErrorApiEvent;
import com.ubogiy.dmytro.probeginapp.event.GetNewsEvent;
import com.ubogiy.dmytro.probeginapp.model.NewsModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.greenrobot.event.EventBus;

public class NewsActivity extends AppCompatActivity {

    private List<NewsModel> news;
    private ListView listViewNews;
    private NewsAdapter adapter;
    private int page;
    public View footerLoading;
    private boolean isLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listViewNews = (ListView) findViewById(R.id.listViewNews);
        news = new ArrayList<>();
        page = 1;
        footerLoading = View.inflate(this, R.layout.footer_loading, null);
        ApiClient.getInstance().requestNews(page);
        //ApiClient.getInstance().requestNewDetail();
        listViewNews.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getLastVisiblePosition() == totalItemCount - 1 && listViewNews.getCount() >= 6 * page && !isLoading) {
                    isLoading = true;
                    page++;
                    ApiClient.getInstance().requestNews(page);
                    listViewNews.addFooterView(footerLoading);
                }
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(ErrorApiEvent errorApiEvent) {
        EventBus.getDefault().removeStickyEvent(errorApiEvent);
        listViewNews.removeFooterView(footerLoading);
        Toast.makeText(this, errorApiEvent.title, Toast.LENGTH_SHORT).show();
    }

    public void onEvent(GetNewsEvent newsEvent) {
        EventBus.getDefault().removeStickyEvent(newsEvent);
        isLoading = false;
        listViewNews.removeFooterView(footerLoading);
        Document html = Jsoup.parse(newsEvent.html);
        parseHtml(html);
        if (page == 1) {
            initListView();
        }
        else {
            addItemsToListView();
        }
    }

    private void addItemsToListView() {
        adapter.addNewsToAdapter(news);
    }

    private void parseHtml(Document html) {
        Elements items = html.select("div[class=item-container]");
        news.clear();
        for (int i = 0; i < items.size(); ++i) {
            NewsModel newsModel = new NewsModel();
            newsModel.details = items.get(i).select("a").attr("href");
            newsModel.imageUrl = items.get(i).select("img").attr("src");
            newsModel.title = items.get(i).select("h5").text();
            news.add(newsModel);
        }
    }

    private void initListView() {
        adapter = new NewsAdapter(this, news);
        listViewNews.setAdapter(adapter);
    }
}
