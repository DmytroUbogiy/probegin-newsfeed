package com.ubogiy.dmytro.probeginapp.apiservice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by dmytroubogiy on 23.06.17.
 */

public interface RestService {

    @GET("news/")
    Call<ResponseBody> getNews(@Query("lcp_page0") int page);

    @GET("news/procrastination/")
    Call<ResponseBody> getNewsDetail();
}
