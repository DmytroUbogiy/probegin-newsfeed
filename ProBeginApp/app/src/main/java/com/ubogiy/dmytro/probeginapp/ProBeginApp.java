package com.ubogiy.dmytro.probeginapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by dmytroubogiy on 24.06.17.
 */

public class ProBeginApp extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
